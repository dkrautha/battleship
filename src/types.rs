use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub(crate) enum Space {
    Empty,
    Hit,
    Miss,
}

#[derive(Debug, PartialEq)]
pub(crate) enum Direction {
    North,
    South,
    East,
    West,
}

#[derive(Debug, PartialEq)]
pub(crate) struct ParseDirectionError;

impl FromStr for Direction {
    type Err = ParseDirectionError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "N" => Ok(Direction::North),
            "S" => Ok(Direction::South),
            "E" => Ok(Direction::East),
            "W" => Ok(Direction::West),
            _ => Err(ParseDirectionError),
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub(crate) struct Battleship {
    pub(crate) body: Vec<(i32, i32)>,
    pub(crate) health: i32,
}

impl Battleship {
    pub(crate) fn new(head: (i32, i32), length: i32, direction: Direction) -> Battleship {
        let mut body = vec![];
        for i in 0..length {
            match direction {
                Direction::North => body.push((head.0, head.1 - i)),
                Direction::South => body.push((head.0, head.1 + i)),
                Direction::East => body.push((head.0 + i, head.1)),
                Direction::West => body.push((head.0 - i, head.1)),
            };
        }
        Battleship {
            body,
            health: length,
        }
    }
}

pub(crate) struct GameBoard {
    pub(crate) width: usize,
    pub(crate) height: usize,
    pub(crate) board: Vec<Vec<Space>>,
    pub(crate) ships: Vec<Battleship>,
}

impl GameBoard {
    pub(crate) fn new(width: usize, height: usize) -> GameBoard {
        let mut board: Vec<Vec<Space>> = vec![];
        for _ in 0..height {
            let row = (0..width).map(|_| Space::Empty).collect();
            board.push(row);
        }
        GameBoard {
            width,
            height,
            board,
            ships: vec![],
        }
    }

    pub(crate) fn is_valid_ship(&self, s: &Battleship) -> bool {
        let s_body = &s.body;
        let x_limit = 0..self.width;
        let y_limit = 0..self.height;
        let mut is_valid: bool = true;
        for (x, y) in s_body {
            if !x_limit.contains(&(*x as usize)) || !y_limit.contains(&(*y as usize)) {
                is_valid = false;
            }
        }
        is_valid
    }

    pub(crate) fn add_ship(&mut self, s: Battleship) -> Result<(), &str> {
        if self.is_valid_ship(&s) {
            self.ships.push(s);
            return Ok(());
        }
        Err("Invalid Ship")
    }

    pub(crate) fn render(&self) {
        let header = format!("+{}+", "-".repeat(self.width));
        println!("{}", header);
        for y in 0..self.height {
            let mut row = String::new();
            for x in 0..self.width {
                match self.board[x][y] {
                    Space::Empty => row.push(' '),
                    Space::Hit => row.push('X'),
                    Space::Miss => row.push('O'),
                }
            }
            println!("|{}|", row);
        }
        println!("{}", header);
    }

    pub(crate) fn ship_render(&self) {
        let header = format!("+{}+", "-".repeat(self.width));
        println!("{}", header);
        for y in 0..self.height {
            let mut row = String::new();
            'rows: for x in 0..self.width {
                for ship in &self.ships {
                    if ship.body.contains(&(x as i32, y as i32)) {
                        row.push('$');
                        continue 'rows;
                    }
                }
                row.push(' ');
            }
            println!("|{}|", row);
        }
        println!("{}", header);
    }

    pub(crate) fn dual_render(&self, other: &GameBoard) {
        let header = format!(
            "+{}+   +{}+",
            "-".repeat(self.width),
            "-".repeat(self.width)
        );
        println!("{}", header);
        for y in 0..self.height {
            let mut row = String::new();
            'ship_render: for x in 0..self.width {
                for ship in &self.ships {
                    if ship.body.contains(&(x as i32, y as i32)) {
                        if self.board[x][y] == Space::Hit {
                            row.push('X');
                        } else {
                            row.push('$');
                        }
                        continue 'ship_render;
                    }
                }
                row.push(' ');
            }

            row.push_str("|   |");

            for x in 0..(self.width) {
                match other.board[x][y] {
                    Space::Empty => row.push(' '),
                    Space::Hit => row.push('X'),
                    Space::Miss => row.push('O'),
                }
            }
            println!("|{}|", row);
        }
        println!("{}", header);
    }
}
